provider "aws" {
    region = "us-east-2"
}
terraform {
     backend   "s3" {
        region = "us-east-2"
        bucket = "terra-bucket116"
        key    = "./terraform.tfstate"
     } 
}
  
data "aws_security_group" "existing" {
    name   = "ec2-rds-3"
    vpc_id = "vpc-06e2f0f3487850df6"
}

resource "aws_security_group_rule" "allow_ssh" {
    type                = "ingress"
    to_port             = 22
    protocol            = "TCP"
    from_port           = 22
    cidr_blocks         = ["0.0.0.0/0"]
    security_group_id   = data.aws_security_group.existing.id
}

resource "aws_security_group_rule" "allow_http" {
    type                = "ingress"
    to_port             = 80 
    protocol            = "TCP"
    from_port           = 80 
    cidr_blocks         = ["0.0.0.0/0"]
    security_group_id   = data.aws_security_group.existing.id

}

resource "aws_instance" "terra-instance" {
     ami                     = "ami-02ca28e7c7b8f8be1"
     instance_type           = "t2.micro"
     key_name                = "docker-key"
     vpc_security_group_ids  = [data.aws_security_group.existing.id]
    tags = {
        Name = "terra-instance"
    }

connection {
        type     = "ssh"
        user     = "ec2-user"
        private_key = file("./docker.pem")
        host     = self.public_ip
    }

   provisioner "remote-exec" {
    inline = [
        "sudo yum install httpd -y",
        "sudo systemctl start httpd",
        "sudo systemctl enable httpd"
        ]
    }


    provisioner "local-exec" {
        command = "sudo echo ${self.public_ip} >> index.html"
    }

    provisioner "file" {
        source = "index.html"
        destination = "/var/www/html/index.html"
    }
}
