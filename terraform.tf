provider "aws" {
  region = "us-east-2"
}


resource "aws_security_group" "my_sg" {
   name = "terra-sg"
   description = "this is my sg" 
   vpc_id = "vpc-06e2f0f3487850df6"
   ingress {
      protocol = "TCP"
      from_port = "22"
      to_port = "22"
      cidr_block = ["0.0.0.0/0"]
   }
    ingress {
      protocol = "TCP"
      from_port = "80"
      to_port = "80"
      cidr_block = ["0.0.0.0/0"]
   }
   egress {
      protocol = "-1"
      from_port = "0"
      to_port = "0"
      cidr_block = ["0.0.0.0/0"]
   }
}
