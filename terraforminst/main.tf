provider "aws" {
   region = "us-east-2" 
}

#vpc

resource "aws_vpc" "new_vpc" {
    cidr_block = var.my_cidr
    tags = {
      Name = "${var.naam}-vpc"
      env = "dev"
    }
}

#subnet-private

resource "aws_subnet" "my_pri_subnet" {
  vpc_id = aws_vpc.new_vpc.id
  cidr_block = var.pri-sub
  tags = {
    Name = "${var.naam}-private-subnet"
  }
}
#subnet-public

resource "aws_subnet" "my_pub_subnet" {
  vpc_id = aws_vpc.new_vpc.id
  cidr_block = var.pub-sub
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.naam}-public-subnet"
  }
}

#internet geteway
resource "aws_internet_gateway" "my-igw" { 
   vpc_id = aws_vpc.new_vpc.id
   tags = {
     name = "${var.naam}-igw"
   }
}
    
 #route table 

resource "aws_route" "route_igw" {
  route_table_id = aws_vpc.new_vpc.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.my-igw.id

}
#security group 

resource "aws_security_group" "my_sg" {
    name = "${var.naam}-sg"
    description = "allow-ssh and http"
    vpc_id = aws_vpc.new_vpc.id
    ingress {
        protocol = "TCP"
        from_port = 22
        to_port  = 22
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "TCP"
        from_port = "80"
        to_port  = "80"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        protocol = "-1"  
        from_port = 0
        to_port = 0
        cidr_blocks = ["0.0.0.0/0"]
    }
}

#instance 
resource "aws_instance" "instance1"{
   ami = var.my_ami
   instance_type = var.my_itype
   key_name = var.my_key
   vpc_security_group_ids = [aws_security_group.my_sg.id]
   subnet_id = aws_subnet.my_pri_subnet.id
     tags = {
      Name = "${var.naam}-1"
     }
}

#instance2
resource "aws_instance" "instance2"{
   ami = var.my_ami
   instance_type = var.my_itype
   key_name = var.my_key
   vpc_security_group_ids = [aws_security_group.my_sg.id]
   subnet_id = aws_subnet.my_pub_subnet.id
     tags = {
      Name = "${var.naam}-2"
     }
}




  
    
 