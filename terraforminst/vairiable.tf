variable "my_cidr"{
   default = "198.168.0.0/16"
}

#name of project 

variable "naam" {
   default = "shubham"

}

#define env 

variable "my_env" {
   default = "devlopment"

}

#private subnet 

variable "pri-sub" {
   default = "198.168.0.0/20"
}
#public subnet 

variable "pub-sub" {
   default = "198.168.16.0/20"
}
#amiid
variable "my_ami"{
   default = "ami-05fb0b8c1424f266b"
}

#instance type 
variable "my_itype" {
   default = "t2.micro"
}

#key pair
variable "my_key" {
   default = "docker-key"
}