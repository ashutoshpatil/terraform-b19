#create iam user 
provider "aws" {
    region = "us-east-2"
}

resource "aws_iam_user" "new_user" {
    name = "ashu"
    path = "/"
    tags = {
      tags  = "ashupatil"

    }
}    

output "iam_user_name" {
    value = aws_iam_user.new_user.name
}

#create buucket 

resource "aws_s3_bucket" "my_bk" {
    bucket = "tejahjgbucket"
    tags = {
       Name = "ravi-bucket" 
    }
}

#s3 policy create

resource "aws_iam_policy" "bucket_policy" {
  name        = "S3BucketPolicyForMyBucket"
  description = "IAM policy for granting PutBucketPolicy action on the S3 bucket"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "s3:PutBucketPolicy",
        Resource = "arn:aws:s3:::tejahjgbucket"
      }
    ]
  })
}

resource "aws_iam_user_policy_attachment" "attachment-policy" {
  user       = aws_iam_user.new_user.name
  policy_arn = aws_iam_policy.bucket_policy.arn
}
